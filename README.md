# LED Buck module HV

![image](xx . jpg)



## Synopsis

Board to convert 2-4S cell voltage (~6-30V) down to run LED strips, COB lights, etc.

Single current regulated output.
Two low side mosfet switches.
Battery voltage monitoring.


## Hardware

- STM32F030F4P6 microcontroller
- PT4115 LED driver (6-30V 1.2A)
- SOT-23 N-FET x 2
- SOD123 diode 
- 68uH inductor


## PCB
- Rev_1: 
- Spike on 3.0V regulator can kill MCU, needs large electrolytic on output to dampen

- Rev_2: 
- Add DIM pulldown
- 


## Issues
- 


## License

MIT license where applicable.  