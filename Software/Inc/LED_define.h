#pragma once

#include <stdio.h>

//------ Defines ------

#define MAX_LED_PERCENT	1.0				//scale brightness 0-100% = 0-1.0 (for 12V battery)
#define TIMER_MULTIPLIER 5.0			//


//------ Enums ------


enum class Type_Board
{
	PT4115,				//1x PT4115 6-30V (reg 18V), 50kHz dimming + 2 FET outputs
	PAM2804,			//2x PAM2804 4-xV input, 1 FET output
};


enum class Type_RF
{
	Receiver,
	Transmitter,
	None,
};


enum class Type_Light
{
	Front_Light,
	Rear_Light,
	Other,
};


//Turn signals (for PT4115)
enum class Side
{
	Left,
	Right,
	Off,
};

#ifdef FRONT_LIGHT
enum class Light_State
{
	Bright,
	Normal,
	Dim,
	Off,
};

enum class Horn_State
{
	On,
	//Short beep or pulse?
	Off,
};

#endif

//------ Board -------



