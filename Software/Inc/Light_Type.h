#pragma once
#include <stdint.h>
#include "LED_define.h"
#include "LED_pattern.h"
#include <macro.h>


#ifdef FRONT_LIGHT
enum class States
{
	Idle,
	First_Button_Press,
	Second_Button_Press,
	Button_Held,
};
#elif REAR_LIGHT
enum class States
{
	Idle,
	First_Button_Press,
	Horn_On,
	Turning_Left,
	Turning_Right,
};
#endif




//Base class
class Light_Type
{
public:
	Light_Type();
	virtual ~Light_Type() {}
	virtual void state_machine(uint16_t buttons) = 0;
	virtual void flash_secondary(void) = 0;
	
	void fix_pattern(uint32_t pattern);
	void next_pattern(void);
	uint16_t run(void);
	uint16_t turn_signal(void);
	
	int testing = 0;
	
private:
	LED_pattern led_main;								//Main LED
	LED_pattern led_signal;								//Turn signals (yellow)
	//not static?
};


//
class Light_Front : public Light_Type
{
public:
	void state_machine(uint16_t buttons); 
	void flash_secondary(void);

	Light_State light() const { return m_light; }
	Horn_State horn() const { return m_horn; }

private:
	Light_State m_light;
	Horn_State m_horn;
	//static LED_pattern led_main;								//Main LED (red)
	//static LED_pattern led_signal;								//Turn signals (yellow)

};


class Light_Rear : public Light_Type
{
public:
	void state_machine(uint16_t buttons); 
	void flash_secondary(void);
private:
	Side m_turn_dir;
	//static LED_pattern led_main;								//Main LED
	
};