#pragma once

#define HIGH 1
#define LOW 0

#define F_CPU 40000000


#include "stdint.h"
#include "stm32f030x6.h"
#include "Delay.h"



static uint32_t PCLK1_Clock_Speed(void)
{
	/* Get PCLK1 frequency */
	uint32_t pclk1 = HAL_RCC_GetPCLK1Freq();
     
	/* Get PCLK1 prescaler */
	if ((RCC->CFGR & RCC_CFGR_PPRE_1) == 0)
	{
		/* PCLK1 prescaler equal to 1 => TIMCLK = PCLK1 */
		return (pclk1);
	}
	else
	{
		/* PCLK1 prescaler different from 1 => TIMCLK = 2 * PCLK1 */
		return (2 * pclk1);
	}
}

//Get APB1 timer frequency
static uint32_t APB1_Timer_Clock_Speed(void)
{
	/* Get PCLK1 frequency */
	uint32_t pclk1 = HAL_RCC_GetPCLK1Freq();
	return (2 * pclk1);
}