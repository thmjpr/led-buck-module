#pragma once

#include "stm32f0xx_hal.h"
#include "stdint.h"
#include "tim.h"

#define delayMicroseconds delay_us

//APB1 timer clock of 20MHz

static inline void setup_delay_us(TIM_HandleTypeDef * htim, uint16_t mhz_clock)
{
	//assert check here
	//assert_param(mhz_clock >= 1000000);
	uint16_t prescaler = mhz_clock / 1000000;

	//Assuming internal CLK divide = 0, can set too somewhere..
	__HAL_TIM_SET_PRESCALER(htim, prescaler);
}

//16-bit maximum delay = 65535 = 65ms
static inline void delay_us(uint16_t us)
{	
	__HAL_TIM_SET_COUNTER(&htim16, 0);				// set the counter value a 0
	while (__HAL_TIM_GET_COUNTER(&htim16) < us) ;	// wait for the counter to reach the us input in the parameter
}