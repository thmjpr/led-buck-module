/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdbool.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
bool led_sts(bool led);
void light_sensor(bool state);

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define Mode_Button_Pin GPIO_PIN_0
#define Mode_Button_GPIO_Port GPIOF
#define Mode_Button_EXTI_IRQn EXTI0_1_IRQn
#define LED_Sts_Pin GPIO_PIN_1
#define LED_Sts_GPIO_Port GPIOF
#define Battery_Pin GPIO_PIN_0
#define Battery_GPIO_Port GPIOA
#define Save_State_Pin GPIO_PIN_1
#define Save_State_GPIO_Port GPIOA
#define PA2_Pin GPIO_PIN_2
#define PA2_GPIO_Port GPIOA
#define NC__Pin GPIO_PIN_3
#define NC__GPIO_Port GPIOA
#define NC___Pin GPIO_PIN_4
#define NC___GPIO_Port GPIOA
#define RF_D_Pin GPIO_PIN_5
#define RF_D_GPIO_Port GPIOA
#define RF_MISO_Pin GPIO_PIN_6
#define RF_MISO_GPIO_Port GPIOA
#define RF_MOSI_Pin GPIO_PIN_7
#define RF_MOSI_GPIO_Port GPIOA
#define PWM3_Pin GPIO_PIN_1
#define PWM3_GPIO_Port GPIOB
#define PWM1_Pin GPIO_PIN_9
#define PWM1_GPIO_Port GPIOA
#define PWM2_Pin GPIO_PIN_10
#define PWM2_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

//#ifdef      //if using front light... need to config somewhere else? if constexpr(
#define Left_Turn_Pin RF_MOSI_Pin
#define Right_Turn_Pin RF_MISO_Pin

#define Horn_Button_Pin RF_MOSI_Pin
#define Light_Button_Pin RF_MISO_Pin
	

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
