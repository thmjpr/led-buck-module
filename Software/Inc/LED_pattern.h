#pragma once

#include <stdio.h>
#include <vector>

enum class Action
{
	Off,
	Hold,
	Rising,
	Falling
};

struct LED_Action 
{
	Action action;			//What action
	uint8_t percent; 		//percent brightness
	uint16_t time; 			//time to sustain action
};

//--pattern
extern std::vector<LED_Action> turn_signal_;


//class

class LED_pattern 
{
public:
	LED_pattern(void);
	void reset(void);			//reset state to 0
	uint16_t run(void);			//update state
	void increment(void);		//increment timer
	void next_pattern(void);	//change to next pattern
	void fix_pattern(std::vector<LED_Action> * pattern);
	void fix_pattern(uint32_t number);

private:
	volatile uint32_t count_main;
	volatile uint32_t count_action;
	volatile bool update_led = false;
	bool frozen = false;					//freeze pattern on fixed
	std::vector<LED_Action> * current_pattern;
	void reset_counters(void); 	//reset counter only
};
