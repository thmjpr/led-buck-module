#include "Light_Type.h"
#include "main.h"



//where is signal running stuff???



//---------------------------------------
//Base functions

Light_Type::Light_Type()
{
	led_signal.fix_pattern(&turn_signal_); //Set yellow LEDs to fixed 50% 2Hz pattern
}

void Light_Type::fix_pattern(uint32_t pattern)
{
	led_main.fix_pattern(pattern);
}

void Light_Type::next_pattern(void)
{
	led_main.next_pattern();
}

uint16_t Light_Type::run(void)
{
	testing = 5;
	return led_main.run();
}

uint16_t Light_Type::turn_signal(void)
{
	return led_signal.run();
}




//----------------------------------------


//Idle
//Turn signal on -> press same button, extend flash time, etc.
//maybe can beep horn very short when turning? every 2s or something

//Button state machine 
void Light_Front::state_machine(uint16_t buttons)
	{
		const uint32_t Horn_Timeout =	400;			//10ms state machine
		const uint32_t Bright_Timeout = 500;
	
		static States state = States::Idle;
		static uint32_t previous_button = 0;
		static uint32_t timeout = 0;
	
	
		switch (state)
		{
		case States::Idle:
			if (buttons)
			{			
				if (single_bit_set(buttons))		//single button
				{
					state = States::First_Button_Press;
					previous_button = buttons;
				}
				else							//multiple buttons
				{
				}
			}
			else								//nothing pressed
			{
				previous_button = 0;
				timeout = 0;
				m_light = Light_State::Normal;
				m_horn = Horn_State::Off;
			}

			break;
	
		//----------------------------	
		case States::First_Button_Press:
			if (buttons == 0)	//if same button pressed and released, very short press..
			{
				if (buttons == Horn_Button_Pin)
				{}
				
				if (buttons == Mode_Button_Pin)
					this->next_pattern();				
				state = States::Idle;
			}
	
			else if(buttons == previous_button)	//same button is being held down
			{
				if (buttons == Horn_Button_Pin)
					m_horn = Horn_State::On;
				if (buttons == Light_Button_Pin)
					m_light = Light_State::Bright;
				if (buttons == Mode_Button_Pin)
					this->next_pattern();
				state = States::Button_Held;
			}
		
			else if(buttons != previous_button)	//second button has been pressed
			{
				//previous_button = first button, can change effect based on which was pressed first
				state = States::Idle;
			}
	
			break;
			
		//----------------------------	
		case States::Button_Held:
			if (buttons == 0)			//released
			{
				m_horn = Horn_State::Off;			//this is covered in idle anyway, could ignore?
				m_light = Light_State::Normal; 
				state = States::Idle;
			}
			
			else if(buttons == previous_button)		//holding the same button
			{
				timeout++;
				if (timeout > Horn_Timeout)
						m_horn = Horn_State::Off;
				if (timeout > Bright_Timeout)
						m_light = Light_State::Dim;
			}
			
			else		//press two, or press one release one, but hard to track that?
			{
				m_horn = Horn_State::Off;
				m_light = Light_State::Dim;
				previous_button = buttons;
				state = States::Second_Button_Press;
			}
			
			break;
	
		//----------------------------	
		case States::Second_Button_Press:
			if (buttons == 0)						//buttons released
			{
				this->next_pattern();
				state = States::Idle;
			}
			
			else if(buttons == previous_button)	//two buttons held
			{	
				m_light = Light_State::Dim;
			}
			
			else									//one button released
			{
			}
			
			break;
		
		default:
			//THROW ERROR
			//error_flash(100);
			state = States::Idle;
			break;
		}//switch
	
	}


void Light_Front::flash_secondary(void)	//bool L/R?
{
	
	
}




//----------------------------------------------------------------------




//state machine 
void Light_Rear::state_machine(uint16_t buttons)
	{
		const uint32_t horn_timeout = 20;
		const uint32_t turn_timeout = 20;
	
		static States state = States::Idle;
		static uint32_t previous_button = 0;
		static uint32_t timeout = 0;
	
		switch (state)
		{
		case States::Idle:
			if (buttons)
			{			
				if (single_bit_set(buttons))		//single button
				{
					state = States::First_Button_Press;
					previous_button = buttons;
				}
				else							//multiple buttons
				{
				}
			}
			else								//nothing pressed
			{
				previous_button = 0;
				timeout = 0;
				m_turn_dir = Side::Off;
				//horn = off
			}

			break;
	
			//----------------------------	
		case States::First_Button_Press:
			if (buttons == 0)	//if same button pressed and released
			{
				if (buttons == Mode_Button_Pin)		//PCB button
				{
					this->next_pattern();		//change pattern
				}
			}	
			break;

		
		default:
			//THROW ERROR?****
			//error_flash(100);
			break;
		}//switch
	
	}

void Light_Rear::flash_secondary(void)	//bool L/R?
{
	
	
}