/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "iwdg.h"
#include "tim.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "LED_pattern.h"
#include "LED_define.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
const Board_Type board = Board_Type::PT4115;

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */


volatile bool button_pressed = false;
volatile bool led_update = false;

volatile uint32_t tick = 0;
LED_pattern led;
void error_flash(uint16_t delay);

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void led_pwm(float percent);
void led_yellow_pwm(int32_t percent, Side sd);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC_Init();
  MX_TIM1_Init();
  MX_TIM14_Init();
  MX_IWDG_Init();
  MX_TIM3_Init();
  MX_TIM16_Init();
  MX_TIM17_Init();
  /* USER CODE BEGIN 2 */

	//Watchdog timeout 40kHz osc -> 40k/8/4095 = ~1s
	//if read ADC battery voltage, etc.
  
	start_adc(); //Begin scanning adc
	HAL_TIM_Base_Start_IT(&htim14); //Start the timer running
	__HAL_DBGMCU_FREEZE_IWDG(); //Freeze the watchdog when debugging and paused
	HAL_Delay(200); //Wait for ADC to update
	cal_vref_int(); //Calibrate internal vref
	
	
	//if powered on shortly after powering off, change to the next mode
	//if powered on after a long time, resume old mode? Or just always use main mode
	//mode could depend on how long you power off for (<1s = 1, 2s = 2, etc.)
	
	
	if (get_vcc_voltage() < 2.90)			//Low battery voltage
	{
		//error_flash(1500);
		//may want to sleep
	}
	
	if (get_battery_voltage() < 10.0)		//can flash yellow lights depending on battery voltage
	{}
	
	if (get_temperature() > 45.0)			//High MCU temperature
	{
		//error_flash(200);
	}
	
	led_sts(false);

	
	//test
	led_yellow_pwm(1, Side::Left);
	led_yellow_pwm(1, Side::Right);
	

	while (1)
	{	  
		//Voltage will be ~3V at idle, unless SRF is blown then it will be 0V
		
		//
		if (button_pressed)
		{
			led.next_pattern(); //increment
			button_pressed = false;
		}

		//Might need moving average as the voltage will spike during PWM and then drop after, if you want to modulate brightness 
		if (adc_complete)
		{
			float batt = get_battery_voltage(); //get battery
			
			if (batt < 10.50)						//If battery < 3.5V per cell
			{
				//Reduce PWM? change mode?
				//prepare for shutdown? low batt?
			}
			else if(batt > 16.00) 
			{
				//error_flash(800);
			}
			else {}
			
			if (get_vcc_voltage() < 2.50)			//Low Vcc
			{
				//error_flash(1000);
			}
			
			
			HAL_IWDG_Refresh(&hiwdg); //Watchdog refresh
			adc_complete = false; //
		}
		
		//Update LED every 10ms
		if (led_update)
		{
			led_pwm(led.run()); //get next brightness from pattern generator
			led_update = false;
		}
	  
		//could add sleep, would need to disable IWDG before entering sleep or it will wake up
	}
	
	
	
	
	
	
	
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1)
	{
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI14
                              |RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL10;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
//Adjust PWM to LED buck circuitry (PT4115)
//TIM1_CH2 is main PWM output
//PWM frequency ~160kHz (?)

void led_pwm(float percent)
{
	//limit to 0-100% brightness
	constrain(&percent, 0, 100);
	
	//convert percent to 0-200 timer range, so 80% maximum PWM duty cycle (200/250).
	percent *= 2;
	percent = percent * MAX_LED_PERCENT; //scale back for debugging or higher input voltage
	
	//__HAL_TIM_GET_AUTORELOAD(&htim1);							//gets the Period set for PWM
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (uint16_t)percent); //set PWM duty cycle
	
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2); //needed to enable timer
}

//Secondary LED low side FET PWM outputs

// need to limit duty cycle and on time
// what frequency? 100kHz+?

void led_yellow_pwm(int32_t percent, Side sd)
{
	constrain(&percent, 0, 100); //Constraint percent: 0-100%
										//12V - 3V / 3R = 3A. 0805 max ~0.1W, ?
	//percent = percent * MAX_LED_PERCENT;		//Limit LED percent range
	
	if (sd == Side::Left)
	{
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (uint16_t)percent);
		HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
	}
	else if (sd == Side::Right)
	{
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, (uint16_t)percent);
		HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_4);
	}
	else {}
}


//Status LED
bool led_sts(bool state)
{
	static bool current;
	
	current = HAL_GPIO_ReadPin(LED_Sts_GPIO_Port, LED_Sts_Pin);
	
	if (state)
		HAL_GPIO_WritePin(LED_Sts_GPIO_Port, LED_Sts_Pin, GPIO_PIN_SET);
	else
	{
		HAL_GPIO_WritePin(LED_Sts_GPIO_Port, LED_Sts_Pin, GPIO_PIN_RESET);
	}
	
	return current;
}



//--------------------------- Interrupts --------------------------

//External interrupt 
//button press
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	static uint32_t last_tick = 0;

	if (GPIO_Pin == Button_Pin)
	{
		if (abs_diff_32(tick, last_tick) > 4)     //if >400ms since last button press
		{
			button_pressed = true;
			last_tick = tick;
		}
	}
}

//Timer (TIM14)
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if (htim == &htim14)        //TIM14 elapsed (40M / 400 / 1000 = 0.01s)
	{
		tick++; //increment 0.01s tick
		led_update = true; //
	}
}


//-------------------------------------------------------------------
//Errors

//Infinite error loop
void error_flash(uint16_t delay)
{
	led_pwm(0);
	led_yellow_pwm(0, Side::Right); led_yellow_pwm(0, Side::Left);
	
	while (1)
	{
		led_sts(!led_sts(true));
		HAL_Delay(delay);
		HAL_IWDG_Refresh(&hiwdg); //Watchdog refresh
	}
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	 /* User can add his own implementation to report the HAL error return state */
	
	error_flash(500);
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
	  /* User can add his own implementation to report the file name and line number,
	     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
