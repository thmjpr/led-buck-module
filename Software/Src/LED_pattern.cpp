//LED

#include "LED_pattern.h"
#include "LED_define.h"
#include "macro.h"
#include "main.h"



//Action, PWM percent, time in 10ms
std::vector<LED_Action> rise_fall {
	{ Action::Rising, 60, 200 },
	{ Action::Hold, 60, 10 },
	{ Action::Falling, 30, 100 },
	{ Action::Hold, 30, 10 },
	//{ Action::Rising, 80, 200 },
};

std::vector<LED_Action> rise_fall_brighter {
	{ Action::Rising, 60, 200 },
	{ Action::Rising, 20, 200 },
};

std::vector<LED_Action> on_with_pulse {
{ Action::Hold, 50, 280 },
{ Action::Hold, 100, 30 },
};

std::vector<LED_Action> on_with_two_pulse {
{ Action::Hold, 40, 280 },
{ Action::Hold, 90, 20 },
{ Action::Hold, 1, 5 },
{ Action::Hold, 80, 20 },
};

std::vector<LED_Action> on_with_two_pulse_lim {
	{ Action::Hold, 45, 380 },
	{ Action::Hold, 80, 20 },
	{ Action::Hold, 20, 5 },
	{ Action::Hold, 75, 20 },
};

std::vector<LED_Action> solid_10 {
	{ Action::Hold, 10, 10 },
};

std::vector<LED_Action> solid_20 {
	{ Action::Hold, 20, 10 },
};

std::vector<LED_Action> solid_40 {
	{ Action::Hold, 40, 10 },
};

std::vector<LED_Action> solid_50 {
{ Action::Hold, 50, 10 },
};

std::vector<LED_Action> simple_flash {
	{ Action::Hold, 4, 60 },
	{ Action::Hold, 100, 100 }
};

std::vector<LED_Action> simple_flash_60 {
	{ Action::Hold, 4, 60 },
	{ Action::Hold, 60, 100 }
}
;

std::vector<LED_Action> turn_signal_ {
	{ Action::Off, 0, 30 },
	{ Action::Hold, 2, 30 }
};

std::vector<LED_Action> pulse_test {
	{ Action::Hold, 20, 150 },
	{ Action::Hold, 70, 20 },
	{ Action::Falling, 15, 20 },
};

std::vector<LED_Action> daytime {
	{ Action::Hold, 5, 210 },
	{ Action::Hold, 100, 25 },
};

std::vector<LED_Action> off {
	{ Action::Off, 0, 10 },
};


//std::initializer_list<std::vector<LED_Action>> patterns = { rise_fall, on_with_pulse, on_with_two_pulse, solid_50, simple_flash};		//
//std::vector<LED_Action> patterns[] = {off, pulse_test, simple_flash, on_with_pulse, on_with_two_pulse, solid_50, rise_fall, off};		//for helmet 4V
//std::vector<LED_Action> patterns[] = {off, pulse_test, daytime, on_with_two_pulse, solid_40, rise_fall, solid_20, off};				//for 12V bike
//std::vector<LED_Action> patterns[] = {rise_fall, on_with_two_pulse_lim, pulse_test, solid_40, simple_flash};						//for rear 12V pannier
std::vector<LED_Action> patterns[] = { rise_fall, on_with_two_pulse_lim, pulse_test, solid_40, simple_flash_60 };						//for front 12V
//std::vector<LED_Action> patterns[] = { off, pulse_test };
//std::vector<LED_Action> patterns[] = { solid_10, solid_20, solid_40, solid_50};		//test


LED_pattern::LED_pattern()
{
	reset();
	current_pattern = &patterns[0];
}

//run current pattern
uint16_t LED_pattern::run() 
{
	float percent = 0;
	static uint32_t pattern_count = 0;
	static float pwm_step = 0;
	static float last_percent = 0;
	
	if (count_main >= (current_pattern->size()))		// if > current pattern length, exception if past bounds
		count_main = 0;
	
	volatile const LED_Action & curr = current_pattern->at(count_main);
	
	percent = last_percent;
	
	if (pattern_count == 0)		//if the first iteration of the step
		{
			pattern_count = curr.time * 2;   	//load how long  (5ms step)
			
			switch(curr.action)
			{
			case Action::Off:
				percent = 0;
				break;
			case Action::Hold:
				percent = curr.percent;
				break;
			case Action::Rising:		//rise/fall is the same code then?
				pwm_step = (curr.percent - last_percent) / (float)(curr.time * 2); 		//find step required.
				break;
			case Action::Falling:
				pwm_step = (curr.percent - last_percent) / (float)(curr.time * 2);  		//find step required.
				break;
			}
		}
	else						//else start counting down
		{
			if ((curr.action == Action::Rising) || (curr.action == Action::Falling))
			{
				percent = last_percent + pwm_step;
			}
			
			pattern_count--;
			
			if (pattern_count == 0)
				count_main++;			//once we reach end of time, move to next pattern step
		}
	
	//std::vector<int>::iterator it = vector.begin();		//iterator
	
	last_percent = percent;
	return (uint16_t)percent;
}

//Go to next pattern
void LED_pattern::next_pattern()
{
	static uint32_t x = 1;
	
	if (++x >= ARR_LEN(patterns))
		x = 0;
	
	reset_counters();		//
	run();					//set internal state to off
	
	current_pattern = &patterns[x];	
}

void LED_pattern::fix_pattern(std::vector<LED_Action> * pattern)
{
	current_pattern = pattern;
	frozen = true;
}

void LED_pattern::fix_pattern(uint32_t number)
{
	//if(number > patterns.max
	current_pattern = &patterns[number];
	frozen = true;
}

//Increment 
void LED_pattern::increment() 
{
	count_main++;
}

//reset current pattern to start and internal timers
void LED_pattern::reset() 
{
	count_main = 0;	
	count_action = 0;
	current_pattern = &off;
}

void LED_pattern::reset_counters() 
{
	count_main = 0;	
	count_action = 0;
}