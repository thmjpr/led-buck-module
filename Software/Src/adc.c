/**
  ******************************************************************************
  * File Name          : ADC.c
  * Description        : This file provides code for the configuration
  *                      of the ADC instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "adc.h"

/* USER CODE BEGIN 0 */

uint32_t adc_dma[NUM_ADC_CHANNELS] = { 0 };          //Buffer for ADC results
uint32_t adc[NUM_ADC_CHANNELS][NUM_ADC_AVGS];        //DMA set to 16-bit transfer
uint16_t vrefint_cal;                              // VREFINT calibration value
float vdda = 0;

int get_average(adc_ch adc_channel);                 //

/* USER CODE END 0 */

ADC_HandleTypeDef hadc;
DMA_HandleTypeDef hdma_adc;

/* ADC init function */
void MX_ADC_Init(void)
{
  ADC_ChannelConfTypeDef sConfig = {0};

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = ENABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.DMAContinuousRequests = ENABLE;
  hadc.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_1;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_VREFINT;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_ADC_MspInit(ADC_HandleTypeDef* adcHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(adcHandle->Instance==ADC1)
  {
  /* USER CODE BEGIN ADC1_MspInit 0 */

  /* USER CODE END ADC1_MspInit 0 */
    /* ADC1 clock enable */
    __HAL_RCC_ADC1_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**ADC GPIO Configuration
    PA0     ------> ADC_IN0
    PA1     ------> ADC_IN1
    */
    GPIO_InitStruct.Pin = Battery_Pin|Save_State_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* ADC1 DMA Init */
    /* ADC Init */
    hdma_adc.Instance = DMA1_Channel1;
    hdma_adc.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_adc.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_adc.Init.MemInc = DMA_MINC_ENABLE;
    hdma_adc.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
    hdma_adc.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
    hdma_adc.Init.Mode = DMA_CIRCULAR;
    hdma_adc.Init.Priority = DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(&hdma_adc) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(adcHandle,DMA_Handle,hdma_adc);

  /* USER CODE BEGIN ADC1_MspInit 1 */

  /* USER CODE END ADC1_MspInit 1 */
  }
}

void HAL_ADC_MspDeInit(ADC_HandleTypeDef* adcHandle)
{

  if(adcHandle->Instance==ADC1)
  {
  /* USER CODE BEGIN ADC1_MspDeInit 0 */

  /* USER CODE END ADC1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_ADC1_CLK_DISABLE();

    /**ADC GPIO Configuration
    PA0     ------> ADC_IN0
    PA1     ------> ADC_IN1
    */
    HAL_GPIO_DeInit(GPIOA, Battery_Pin|Save_State_Pin);

    /* ADC1 DMA DeInit */
    HAL_DMA_DeInit(adcHandle->DMA_Handle);
  /* USER CODE BEGIN ADC1_MspDeInit 1 */

  /* USER CODE END ADC1_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */

//Start continuous conversions
void start_adc(void)
{
    //May need to wait until VRef int is stable
	//Will need recalibration if temperature varies
	
	//Calibrate ADC (occur after MX_ADC_INIT)
	HAL_ADCEx_Calibration_Start(&hadc);      
	
	//Start circular DMA
	HAL_ADC_Start_DMA(&hadc, &adc_dma[0], NUM_ADC_CHANNELS);	
}


//******************************************************************************
int adc_current_val(adc_ch adc_channel)
{
	return get_average(adc_channel);
}

//******************************************************************************
int get_average(adc_ch adc_channel)
{
	uint32_t avg = 0;
	
	for (int i = 0; i < NUM_ADC_AVGS; i++)
	{
		avg += adc[adc_channel][i];	
	}
	
	return (avg / NUM_ADC_AVGS);     //return average
}

//?
float get_vcc_voltage(void)
{
	return vdda;
}

//******************************************************************************
float get_battery_voltage(uint32_t divider)
{	
	float volts;
	uint32_t adc_avg = get_average(ADC_VBattery);
	
	//need to be scaled based on internal reference voltage
	//VDDA = 3.0V (or whatever) x vrefint cal / vrefint data * 10:10 resistor divider
	volts = (vdda / ADC_FULL_SCALE) * adc_avg * divider;
	
	return volts;
}

//******************************************************************************
float get_savestate_voltage(void)
{	
	uint32_t adc_avg = get_average(ADC_VSaveState);
	
	//need to be scaled based on internal reference voltage
	float volts = (vdda / ADC_FULL_SCALE) * adc_avg * 1;
	
	return volts;
}

//******************************************************************************
uint32_t get_savestate_val(void)
{	
	return get_average(ADC_VSaveState);
}


//******************************************************************************
float get_temperature(void)
{
	int v_temp = get_average(ADC_TEMP);
	int vref = get_average(ADC_INT_REF);
	
	float temperature = (v_temp * (*VREFINT_CAL_ADDR)) / vref - (*TEMP30_CAL_ADDR);
	temperature *= 110 - 30;
	temperature /= (*TEMP110_CAL_ADDR - *TEMP30_CAL_ADDR);
	temperature += 30;
	
	return temperature;
}

//******************************************************************************
void cal_vref_int(void)
{
	//injected current 5mA max
	vrefint_cal = (*VREFINT_CAL_ADDR);         // read VREFINT_CAL_ADDR memory location
	
	//need 4us when measuring vrefint
	//1/14MHz = 56 cycles
	int vrefint_data = get_average(ADC_INT_REF);  	//adc_current_val(ADC_CHANNEL_VREFINT);	// read vrefint adc channel
	
	vdda = (3.300 * vrefint_cal) / vrefint_data;  	//Calculate Vdda current voltage using reference.
	//vdda = SUPPLY_VOLTAGE;
}

//---------------------- Interrupts ---------------------------

//******************************************************************************
//ADC sequence conversion complete
//could get it to average in HW? if its available
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	static uint32_t x = 0;
		
	for (int y = 0; y < NUM_ADC_CHANNELS; y++)
	{
		adc[y][x] = adc_dma[y];
	}
	
	if (++x >= NUM_ADC_AVGS)
	{
		adc_complete = true;
		x = 0;
	}
}

void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc)
{
	int test = 0;
	test++;
	
	Error_Handler();
}

void ADC_Interrupt(void)
{
	//not used
}


// Interrupt when ADC value goes above a certain level
// should be set very high
void HAL_ADC_LevelOutOfWindowCallback(ADC_HandleTypeDef* hadc)
{
//	led_pwm(0);      //disable output
	
}



/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
