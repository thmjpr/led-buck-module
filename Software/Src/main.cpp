/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "iwdg.h"
#include "tim.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
//#include "LED_pattern.h"		//Lighting patterns
#include "LED_define.h"			//
#include "Light_Type.h"			//Various light config
#include "macro.h"
#include "hal_gpio.h"
#include "Arduino.h"
#include "Lib/arduino-libs-manchester-master/Manchester.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
HAL_GPIO_PIN(Save_State, A, 1)			//can clean up later
HAL_GPIO_PIN(LED_Red, A, 9)
HAL_GPIO_PIN(RxD, A, 5)
//use defines? or include in a seperate class..
HAL_GPIO_PIN(Horn1, A, 10)
HAL_GPIO_PIN(Horn2, B, 1)

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
	
//Configuration

#ifdef FRONT_LIGHT
	const Type_Light CFG_Light = Type_Light::Front_Light;
#else
	const Type_Light CFG_Light = Type_Light::Rear_Light;
#endif
const Type_Board CFG_Board = Type_Board::PT4115;
const Type_RF CFG_RF = Type_RF::Receiver;

//If configuration = Front light, Rear light

//
volatile uint16_t buttons_pressed = 0;
volatile bool led_update = false;
static Side turn_direction = Side::Off;			//turn which direction


//Flash patterns
//static LED_pattern led_main;								//Main LED (red)
//static LED_pattern led_signal;								//Turn signals (yellow)
Manchester rf(APB1_Timer_Clock_Speed());			//Wireless 433MHz RF module

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void led_pwm(int32_t percent);
void led_yellow_pwm(int32_t percent, Side sd);
void horn(bool on);
void error_flash(uint16_t delay);
uint32_t read_buttons(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
	/* USER CODE BEGIN 1 */
	uint32_t turn_signal_timeout = 0;
	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_ADC_Init();
	MX_TIM1_Init();
	MX_TIM14_Init();
	MX_TIM16_Init();
	MX_IWDG_Init();
	MX_TIM3_Init();
	/* USER CODE BEGIN 2 */
  
	led_pwm(0);						//Light off
	start_adc();					//Begin scanning adc
	__HAL_DBGMCU_FREEZE_IWDG();		//Freeze the watchdog when debugging and paused
	HAL_Delay(200);					//Wait for ADC to update
	HAL_IWDG_Refresh(&hiwdg);		//Watchdog timeout 40kHz osc -> 40k/16/4095 = ~1.6s
	cal_vref_int();					//Calibrate	internal vref
	HAL_TIM_Base_Start_IT(&htim14); //Start update timer running, interrupt enabled
	HAL_TIM_Base_Start(&htim16);	//Delay timer
	setup_delay_us(&htim16, APB1_Timer_Clock_Speed());		//Set for 1us
	//HAL_TIM_Base_Start(&htim17); //RF receive timer
	
//base class ptr not working
#ifdef FRONT_LIGHT
	Light_Front light;
#else
	Light_Rear light;
#endif // FRONT_LIGHT

	light.run();
	
	//if powered on shortly after powering off, change to the next mode
	//if powered on after a long time, resume old mode? Or just always use main mode
	//mode could depend on how long you power off for (<1s = 1, 2s = 2, etc.)	
	
	//Set function based on off time
	//Voltage quickly drops to 0.4V (200ms) to 0.24V (1s), then there is slight leakage after power on
	uint32_t x = get_savestate_val();
	volatile int save_state_time = 0;
	if (x > VOLT_TO_ANLG(0.80))			{save_state_time = 0;}
	else if (x > VOLT_TO_ANLG(0.50))	{save_state_time = 1;}
	else if (x > VOLT_TO_ANLG(0.35))	{save_state_time = 2;}
	else if (x > VOLT_TO_ANLG(0.25))	{save_state_time = 3;}
	else if (x > VOLT_TO_ANLG(0.10))	{save_state_time = 4;}	//0.1V seems to be the lowest reading due to leakage
	else								{save_state_time = 5;}
		
	HAL_GPIO_Save_State_in();		//Change to input pin
	HAL_GPIO_Save_State_pullup();	//Enable pullup to charge capacitor, for next power off
		
	//Set voltage divider based on board type
	uint32_t divider = 0;
	if (CFG_Board == Type_Board::PT4115) {	divider = 11; }	
	if (CFG_Board == Type_Board::PAM2804) { divider = 2; }
	
	if (CFG_RF == Type_RF::Receiver) {}
	if (CFG_RF == Type_RF::Transmitter) {}
	
	
	HAL_IWDG_Refresh(&hiwdg);			//Watchdog refresh
	
	//have _Startup_light_ function? 
	//so front light startup wont have turn signals to flash
	
	if (get_vcc_voltage() < 2.70)		//Low Vcc for some reason
	{
		error_flash(1500);
	}
	
	float vbatt = get_battery_voltage(divider);
	
	if (vbatt < 5.0)		//Debugging or battery dead
	{
//		error_flash(2000);
	}
	
	if (vbatt < 11.0)		//Starting to get low (3.7V/cell)
	{
		//light->low_voltage
		//light->flash_secondary?
		led_yellow_pwm(1, Side::Right);		//put this somewhere else
		HAL_Delay(200);
	}
	
	if (vbatt < 10.0)		//Almost dead (3.3V/cell)
	{
		led_yellow_pwm(0, Side::Right);
		HAL_Delay(200);
		led_yellow_pwm(1, Side::Right);
		HAL_Delay(200);
	}
	led_yellow_pwm(0, Side::Off);
	

	//Show which state was selected with flashes
	for (int k = 0; k < save_state_time; k++)
	{
		HAL_IWDG_Refresh(&hiwdg);			//Watchdog refresh
		led_yellow_pwm(1, Side::Left);
		HAL_Delay(300);
		led_yellow_pwm(0, Side::Left);
		HAL_Delay(300);
	}

	//Choose pattern based on save state pin, how long switch was off
	light.fix_pattern(save_state_time);
	
	led_sts(false);						//onboard status LED off
	
	//rf.setup(Txpin, Rxpin, 2);		//What is 2?
	

	while (1)
	{	  
		//
		if (adc_complete)
		{
			float batt = get_battery_voltage(divider); //get battery
			
			if (batt < 10.50)						//If battery < 3.5V per cell
			{
				//light->low volt
				
				//Reduce PWM? change mode?
				//prepare for shutdown? low batt?
			}
			else if(batt > 16.00) 
			{
				error_flash(800);
			}
			else {}
			
			if (get_vcc_voltage() < 2.50)			//Low Vcc
			{
				error_flash(1500);
			}
			
			if (get_temperature() > 45.0)			//High MCU temperature
			{
				error_flash(200);
			}
			
			HAL_IWDG_Refresh(&hiwdg); //Watchdog refresh
			adc_complete = false; //
		}
		
		//Update LEDs + state machine every 10ms
		if (led_update)
		{	
			led_update = false;
						
			//Button press handler
			buttons_pressed = read_buttons();
			light.state_machine(buttons_pressed);
			
			//Main LED			
			if(light.light() == Light_State::Normal)							//If in normal pattern
				led_pwm(light.run());											//get next brightness from pattern generator
			else																//else in override
			{
				if (light.light() == Light_State::Bright)
					led_pwm(95);
				if (light.light() == Light_State::Dim)
					led_pwm(10);
				if (light.light() == Light_State::Off)
					led_pwm(0);
			}
			
			//Horn
			if (light.horn() == Horn_State::On)
				horn(true);
			else
				horn(false);
			
			
			if(CFG_Light == Type_Light::Rear_Light)							//Yellow LEDs on rear
				led_yellow_pwm(light.turn_signal(), turn_direction);
				
			//else if(CFG_Light == Type_Light::Front_Light)					//Horn on fron
			//	horn(0);		//run horn??
			
		}
	  
		//could add sleep, would need to disable IWDG before entering sleep or it will wake up
	}
	
	

	
	
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Initializes the RCC Oscillators according to the specified parameters
	* in the RCC_OscInitTypeDef structure.
	*/
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI | RCC_OSCILLATORTYPE_LSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL10;
	RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB buses clocks
	*/
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
	                            | RCC_CLOCKTYPE_PCLK1;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
	{
		Error_Handler();
	}
}

/* USER CODE BEGIN 4 */
//Adjust PWM to LED buck circuitry (PT4115)
//TIM1_CH2 is main PWM output
//PWM frequency ~160kHz (?)

void led_pwm(int32_t percent)
{
	//limit to 0-100% brightness
	constrain(&percent, 0, 100);
	//convert percent to 0-200 timer range
	percent *= 2;
	//percent = percent * MAX_LED_PERCENT; //scale back for debugging or higher input voltage
	
	//__HAL_TIM_GET_AUTORELOAD(&htim1);							//gets the Period set for PWM
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (uint16_t)percent); //set PWM duty cycle
	
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2); //needed to enable timer
}

//Secondary LED low side FET PWM outputs

// need to limit duty cycle and on time
// what frequency? 100kHz+?

void led_yellow_pwm(int32_t percent, Side sd)
{
	constrain(&percent, 0, 3);					//Constraint percent: 0-3
												//12V - 3V / 3R = 3A. 0805 max ~0.1W, ?
	//convert percent to 0-200 timer range
	percent *= 2;
	//percent = percent * MAX_LED_PERCENT;		//Limit LED percent range
	
	if (sd == Side::Left)
	{
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (uint16_t)percent);
		HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
	}
	else if (sd == Side::Right)
	{
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, (uint16_t)percent);
		HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_4);
	}
	else	//Off
	{
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, 0);
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, 0);
	}
}


//---------------------
// Horn only used on front light

//Could try PWM the horn for quieter version??? <1s would be quiet or something

void horn(bool on)
{
	static int32_t timeout = 0;		//backup timeout? its already handled in state machine
	
	if (on)
	{
		HAL_GPIO_Horn1_write(true);
		HAL_GPIO_Horn2_write(true);
	}
	else
	{
		HAL_GPIO_Horn1_write(false);
		HAL_GPIO_Horn2_write(false);
	}
}


//Status LED
bool led_sts(bool state)
{
	static bool current;
	
	current = HAL_GPIO_ReadPin(LED_Sts_GPIO_Port, LED_Sts_Pin);
	
	if (state)
		HAL_GPIO_WritePin(LED_Sts_GPIO_Port, LED_Sts_Pin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(LED_Sts_GPIO_Port, LED_Sts_Pin, GPIO_PIN_RESET);
	
	return current;
}

uint32_t read_buttons(void)
{
	static uint32_t last_press = 0xFFFFFFFF;
	static uint32_t last_rf_data = 0xFFFFFFFF;
	uint32_t tick = HAL_GetTick();
	uint32_t buttons = 0;

	last_press = tick;
	buttons = ~(GPIOA->IDR) & (Left_Turn_Pin | Right_Turn_Pin);
	buttons |= ~(GPIOF->IDR) & (Mode_Button_Pin);
	
	
	//Button pressed
	if (0)//(GPIO_Pin == Left_Turn_Pin) | (GPIO_Pin == Right_Turn_Pin))
	{
		if (abs_diff_32(tick, last_press) > 100)     //if >100ms since last button press
		{
			//also needed to allow 10ms loop to process press, avoid race. but may need tweaking for double press?
			last_press = tick;
			buttons_pressed = ~(GPIOA->IDR) & (Left_Turn_Pin | Right_Turn_Pin);
			buttons_pressed |= ~(GPIOF->IDR) & (Mode_Button_Pin);
			//EXTI0 = PA0 and PF0, shared
			//GPIOA->IDR;
		}
	}	
	
	return buttons;
}



//--------------------------- Interrupts --------------------------

//External interrupt 

//GPIO_Pin is useless value, need to actually check it i think?
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	static uint32_t last_press = 0xFFFFFFFF;
	static uint32_t last_rf_data = 0xFFFFFFFF;
	uint32_t tick = HAL_GetTick();

	//RF data pin rising edge - Interrupt not used currently
	if (GPIO_Pin == RF_D_Pin)
	{
		if (abs_diff_32(tick, last_rf_data) > 4)		//if > 2ms since last RF data, timeout
		{}		//reset timeout?
		else
		{
			//update RF state machine?
		}
	}

}

//Timer (TIM14)
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if (htim == &htim14)        //TIM14 elapsed (20M / 100 / 1000 = 0.005s)
	{
		//tick++; //increment 0.01s tick
		led_update = true; //
	}
	else if(htim == &htim16)	//used for timing only
	{
	}
	else if(htim == &htim17)	//Manchester
	{
		rf.timer_interrupt();	//
	}
}


//-------------------------------------------------------------------
//Errors

//Infinite error loop
void error_flash(uint16_t delay)
{
	led_pwm(0);
	led_yellow_pwm(0, Side::Right); led_yellow_pwm(0, Side::Left);
	//horn off
	
	while (1)
	{
		HAL_IWDG_Refresh(&hiwdg); //Watchdog refresh
		led_sts(!led_sts(true));
		HAL_Delay(delay);
	}
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	   /* User can add his own implementation to report the HAL error return state */
	
	error_flash(500);
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
		/* User can add his own implementation to report the file name and line number,
		   tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
